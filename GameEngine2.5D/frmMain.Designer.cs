﻿namespace Flat3D
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlViewport = new System.Windows.Forms.Panel();
            this.pnlMap = new System.Windows.Forms.Panel();
            this.pnlData = new System.Windows.Forms.Panel();
            this.chbSmoothWalls = new System.Windows.Forms.CheckBox();
            this.lblPlayerAngle = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPlayerY = new System.Windows.Forms.Label();
            this.lblPlayerX = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlData.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlViewport
            // 
            this.pnlViewport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlViewport.Location = new System.Drawing.Point(12, 12);
            this.pnlViewport.Name = "pnlViewport";
            this.pnlViewport.Size = new System.Drawing.Size(640, 480);
            this.pnlViewport.TabIndex = 0;
            this.pnlViewport.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlViewport_Paint);
            // 
            // pnlMap
            // 
            this.pnlMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMap.Location = new System.Drawing.Point(658, 12);
            this.pnlMap.Name = "pnlMap";
            this.pnlMap.Size = new System.Drawing.Size(256, 256);
            this.pnlMap.TabIndex = 1;
            this.pnlMap.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMap_Paint);
            this.pnlMap.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlMap_MouseClick);
            // 
            // pnlData
            // 
            this.pnlData.Controls.Add(this.chbSmoothWalls);
            this.pnlData.Controls.Add(this.lblPlayerAngle);
            this.pnlData.Controls.Add(this.label3);
            this.pnlData.Controls.Add(this.lblPlayerY);
            this.pnlData.Controls.Add(this.lblPlayerX);
            this.pnlData.Controls.Add(this.label2);
            this.pnlData.Controls.Add(this.label1);
            this.pnlData.Location = new System.Drawing.Point(658, 283);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(255, 112);
            this.pnlData.TabIndex = 2;
            // 
            // chbSmoothWalls
            // 
            this.chbSmoothWalls.AutoSize = true;
            this.chbSmoothWalls.Enabled = false;
            this.chbSmoothWalls.Location = new System.Drawing.Point(7, 46);
            this.chbSmoothWalls.Name = "chbSmoothWalls";
            this.chbSmoothWalls.Size = new System.Drawing.Size(91, 17);
            this.chbSmoothWalls.TabIndex = 6;
            this.chbSmoothWalls.Text = "Smooth Walls";
            this.chbSmoothWalls.UseVisualStyleBackColor = true;
            this.chbSmoothWalls.Visible = false;
            this.chbSmoothWalls.CheckedChanged += new System.EventHandler(this.chbSmoothWalls_CheckedChanged);
            // 
            // lblPlayerAngle
            // 
            this.lblPlayerAngle.AutoSize = true;
            this.lblPlayerAngle.Location = new System.Drawing.Point(79, 30);
            this.lblPlayerAngle.Name = "lblPlayerAngle";
            this.lblPlayerAngle.Size = new System.Drawing.Size(24, 13);
            this.lblPlayerAngle.TabIndex = 5;
            this.lblPlayerAngle.Text = "n/a";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Player Angle:";
            // 
            // lblPlayerY
            // 
            this.lblPlayerY.AutoSize = true;
            this.lblPlayerY.Location = new System.Drawing.Point(79, 17);
            this.lblPlayerY.Name = "lblPlayerY";
            this.lblPlayerY.Size = new System.Drawing.Size(24, 13);
            this.lblPlayerY.TabIndex = 3;
            this.lblPlayerY.Text = "n/a";
            // 
            // lblPlayerX
            // 
            this.lblPlayerX.AutoSize = true;
            this.lblPlayerX.Location = new System.Drawing.Point(79, 4);
            this.lblPlayerX.Name = "lblPlayerX";
            this.lblPlayerX.Size = new System.Drawing.Size(24, 13);
            this.lblPlayerX.TabIndex = 2;
            this.lblPlayerX.Text = "n/a";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Player Y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player X:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 503);
            this.Controls.Add(this.pnlData);
            this.Controls.Add(this.pnlMap);
            this.Controls.Add(this.pnlViewport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "GameEngine2.5D by Andrey Zhmakin";
            this.pnlData.ResumeLayout(false);
            this.pnlData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlViewport;
        private System.Windows.Forms.Panel pnlMap;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPlayerY;
        private System.Windows.Forms.Label lblPlayerX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPlayerAngle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chbSmoothWalls;
    }
}

