﻿//
// Written by Andrey Zhmakin, 2015
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat3D.Geometry
{
    class Point
    {
        public double X;
        public double Y;

        public Point(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }


        public double distanceTo(double x, double y)
        {
            double dX = this.X - x;
            double dY = this.Y - y;

            return Math.Sqrt( dX * dX + dY * dY );
        }

    }
}
