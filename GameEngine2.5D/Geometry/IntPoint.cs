﻿//
// Written by Andrey Zhmakin, 2015
//

using System;

namespace Flat3D.Geometry
{
    class IntPoint
    {
        public int X;
        public int Y;

        public IntPoint()
        {
            this.X = 0;
            this.Y = 0;
        }

        public IntPoint(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        

        public int distanceTo(IntPoint point)
        {
            long dX = this.X - point.X;
            long dY = this.Y - point.Y;

            return (int) Math.Round(Math.Sqrt( dX * dX + dY * dY ));
        }


        internal double distanceTo(int x, int y)
        {
            long dX = this.X - x;
            long dY = this.Y - y;

            return Math.Sqrt(dX * dX + dY * dY);
        }
    }
}
