﻿//
// Written by Andrey Zhmakin, 2015
//

namespace Flat3D.Geometry
{
    class IntGeometry
    {
        public static int normalizeDegrees(int angle)
        {
            return (4096 + angle % 4096) % 4096;
        }
    }
}
