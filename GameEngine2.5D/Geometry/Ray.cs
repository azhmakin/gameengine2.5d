﻿//
// Written by Andrey Zhmakin, 2015
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat3D.Geometry
{
    public class Ray : Line
    {
        private double x1;
        private double y1;
        private double dX;
        private double dY;

        public Ray(double x1,
                   double y1,
                   double x2,
                   double y2) :
            base(x1, y1, x2, y2)
        {
            this.x1 = x1;
            this.y1 = y1;

            this.dX = x2 - x1;
            this.dY = y2 - y1;
            double dd = Math.Sqrt(this.dX * this.dX + this.dY * this.dY);

            this.dX /= dd;
            this.dY /= dd;
        }


        public override bool Contains(double x, double y)
        {
            if (this.x1 == x && this.y1 == y)
            {
                return true;
            }
            else if (base.Contains(x, y))
            {
                // TODO: Check direction!

                double dX = x - this.x1;
                double dY = y - this.y1;

                double dd = Math.Sqrt(dX * dX + dY * dY);

                dX /= dd;
                dY /= dd;

                return Math.Abs(dX - this.dX) <= 0.0001 && Math.Abs(dY - this.dY) <= 0.0001;
            }
            else
            {
                return false;
            }
        }
    }
}
