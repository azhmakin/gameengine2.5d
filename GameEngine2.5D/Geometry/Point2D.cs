﻿//
// Written by Andrey Zhmakin, 2015
//

using System;

namespace Flat3D.Geometry
{
    class Point2D
    {
        private double x;
        private double y;


        public Point2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }


        public Point2D(Point2D point)
        {
            this.x = point.x;
            this.y = point.y;
        }
        

        public double DistanceTo(double x, double y)
        {
            double dX = this.X - x;
            double dY = this.Y - y;

            return Math.Sqrt(dX * dX + dY * dY);
        }


        public double X
        {
            get
            {
                return this.x;
            }

            set
            {
                this.x = value;
            }
        }


        public double Y
        {
            get
            {
                return this.y;
            }

            set
            {
                this.y = value;
            }
        }
    }
}
