﻿//
// Written by Andrey Zhmakin, 2015
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat3D.Geometry
{
    public class Line
    {
        protected double a, b, c;

        public Line(double x1,
                    double y1,
                    double x2,
                    double y2)
        {
            this.Initialize(x1, y1, x2, y2);
        }


        private void Initialize(double x1,
             double y1,
             double x2,
             double y2)
        {
            System.Diagnostics.Debug.Assert(!(x1 == x2 && y1 == y2));

            double dX = x2 - x1;

            if (dX == 0)
            {
                this.a = 1.0;
                this.b = 0;
            }
            else
            {
                double gY = y1 - y2;
                double k;

                if (gY == 0 || (k = gY / dX) == 0)
                {
                    this.a = 0;
                    this.b = 1;
                }
                else
                {
                    double sqrK = k * k;

                    this.a = Math.Sqrt(sqrK / (sqrK + 1.0));
                    this.b = this.a / k;
                }
            }

            double t1 = this.a * x1 + this.b * y1;
            double t2 = this.a * x2 + this.b * y2;

            //assert ( t1 == t2 ) : "t1 = " + t1 + ", t2 = " + t2 + ", avg = " + 0.5 * ( t1 + t2 );

            this.c = -0.5 * (t1 + t2);
        }


        public virtual bool Contains(double x, double y)
        {
            return Math.Abs(this.DistanceSigned(x, y)) <= 1.0e-4;
        }


        public double DistanceSigned(double x, double y)
        {
            return this.a * x + this.b * y + this.c;
        }
    }
}
