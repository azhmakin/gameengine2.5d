﻿//
// Written by Andrey Zhmakin, 2015
//

using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Windows.Forms;
using Flat3D.Geometry;

namespace Flat3D
{
    public partial class frmMain :
        Form,
        IMessageFilter
    {
        private bool bitSmoothWalls = false;
        private Image wallTexture = null;

        private BufferedGraphicsContext context;
        private BufferedGraphics grafx;

        public frmMain() : base()
        {
            InitializeComponent();

            Application.AddMessageFilter(this);
            this.FormClosed += new FormClosedEventHandler(this.frmMain_FormClosed);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            this.pnlViewport.Paint += new PaintEventHandler(pnlViewport_Paint);
            this.pnlMap.Paint += new PaintEventHandler(pnlMap_Paint);

            this.context = BufferedGraphicsManager.Current;
            context.MaximumBuffer = new Size(this.pnlViewport.Width, this.pnlViewport.Height);
            grafx = context.Allocate(this.pnlViewport.CreateGraphics(),
                 new Rectangle(0, 0, this.pnlViewport.Width, this.pnlViewport.Height));

            this.paintScene(grafx.Graphics);
        }


        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.RemoveMessageFilter(this);
        }


        public bool PreFilterMessage(ref Message m)
        {
            Player player = Program.player;

            bool modified = false;

            if (m.Msg == 0x0100)
            {
                Keys key = (Keys) m.WParam.ToInt32();

                modified = true;

                switch (key)
                {
                    case Keys.W:
                    case Keys.Up:
                        player.moveForward(Program.map);
                        pnlMap.Refresh();
                        pnlViewport.Refresh();
                        break;

                    case Keys.S:
                    case Keys.Down:
                        player.moveBackward(Program.map);
                        pnlMap.Refresh();
                        pnlViewport.Refresh();
                        break;

                    case Keys.Left:
                        player.turnLeft();
                        pnlMap.Refresh();
                        pnlViewport.Refresh();
                        break;

                    case Keys.Right:
                        player.turnRight();
                        pnlMap.Refresh();
                        pnlViewport.Refresh();
                        break;

                    case Keys.A:
                        player.StrafeLeft(Program.map);
                        pnlMap.Refresh();
                        pnlViewport.Refresh();
                        break;

                    case Keys.D:
                        player.StrafeRight(Program.map);
                        pnlMap.Refresh();
                        pnlViewport.Refresh();
                        break;

                    case Keys.Space:
                        if (this.jumpWorker == null)
                        {
                            this.jumpWorker = new JumpWorker(player, pnlMap);
                        }

                        if (!this.jumpWorker.InJump())
                        {
                            Thread workerThread = new Thread(this.jumpWorker.DoWork);
                            workerThread.Start();
                        }
                        break;
/*
                    case Keys.PageUp:
                        if (player.Height < WALL_HEIGHT)
                        {
                            player.Height += 1;
                        }
                        pnlMap.Invalidate();
                        break;

                    case Keys.PageDown:
                        if (player.Height > 0)
                        {
                            player.Height -= 1;
                        }
                        pnlMap.Invalidate();
                        break;
*/
                    default:
                        modified = false;
                        break;
                }
            }

            if (modified)
            {
                this.updateData();
                this.pnlData.Update();
                this.paintScene(grafx.Graphics);
            }

            return false;
        }

        private JumpWorker jumpWorker = null;

        class JumpWorker
        {
            public JumpWorker(Player player, Panel pnlMap)
            {
                this.player = player;
                this.pnlMap = pnlMap;
            }

            private bool inJump = false;
            private Player player;
            private bool up = true;
            private int offset = 0;
            private Panel pnlMap;

            public void DoWork()
            {
                inJump = true;
                up = true;
                offset = 0;

                for ( ; ; )
                {
                    if (up)
                    {
                        offset++;

                        if (player.Height < WALL_HEIGHT)
                        {
                            player.Height++;
                        }

                        if (offset >= 5)
                        {
                            up = false;
                        }
                    }
                    else
                    {
                        offset--;

                        if (player.Height > 0)
                        {
                            player.Height--;
                        }

                        if (offset <= 0)
                        {
                            Debug.Assert(player.Height == Player.INIT_HEIGHT, "player.Height = " + player.Height);
                            inJump = false;
                            return;
                        }
                    }
                    
                    //pnlMap.Invalidate();

                    Thread.Sleep(100);
                }
            }

            public bool InJump()
            {
                return this.inJump;
            }
        }


        private void updateData()
        {
            this.lblPlayerX.Text = Program.player.X.ToString();
            this.lblPlayerY.Text = Program.player.Y.ToString();
            this.lblPlayerAngle.Text = Program.player.Angle.ToString();
        }


        private void paintMap()
        {
            //e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            Graphics g = pnlMap.CreateGraphics();
            g.SmoothingMode = SmoothingMode.AntiAlias;

            Map map = Program.map;

            // Draw maze
            Brush blackBrush = new SolidBrush(Color.Black);

            double scale = 25.0;

            for (int y = 0; y < map.height; y++)
            {
                for (int x = 0; x < map.height; x++)
                {
                    if (map.block(x, y) != 0)
                    {
                        g.FillRectangle(blackBrush,
                            new Rectangle(
                                Convert.ToInt32(x * scale),
                                Convert.ToInt32(y * scale),
                                Convert.ToInt32(scale),
                                Convert.ToInt32(scale))
                            );
                    }
                }
            }

            blackBrush.Dispose();

            // Draw player

            Player player = Program.player;

            int playerX = Convert.ToInt32(player.X * scale / Map.BLOCK_SIZE);
            int playerY = Convert.ToInt32(player.Y * scale / Map.BLOCK_SIZE);

            // Draw vision ray
            {
                int endX = playerX + 1000;
                int endY = playerY;

                rotate(ref endX, ref endY, player.Angle, playerX, playerY);

                g.DrawLine(new Pen(Color.Blue), playerX, playerY, endX, endY);

                Flat3D.Geometry.Point2D intersection = map.IntersectionWithWall(player.X, player.Y, Player.toRad(player.Angle));

                if (intersection != null)
                {
                    int cx = Convert.ToInt32(Math.Round(intersection.X * scale / Map.BLOCK_SIZE));
                    int cy = Convert.ToInt32(Math.Round(intersection.Y * scale / Map.BLOCK_SIZE));

                    Pen greenPen = new Pen(Color.Green);
                    g.DrawEllipse(greenPen, cx - 3, cy - 3, 5, 5);
                    greenPen.Dispose();
                }
            }

            // Draw player
            Brush redBrush = new SolidBrush(Color.Red);
            g.FillEllipse(redBrush, playerX - 3, playerY - 3, 6, 6);
            redBrush.Dispose();

            int targetX = playerX + 20;
            int targetY = playerY;

            rotate(ref targetX, ref targetY, player.Angle, playerX, playerY);

            Pen redPen = new Pen(Color.Red);
            drawArrow(g, redPen, playerX, playerY, targetX, targetY);
            redPen.Dispose();
        }


        const int VIEWPORT_ANGLE = 1024; // 90deg
        const int WALL_HEIGHT = 16;
        const int DISTANCE_TO_VIEWPORT = 20;
        const int DISTANCE_OF_VIEW = Map.BLOCK_SIZE * 5;


        private static Pen shadeOfBlack(int distance)
        {
            int c;

            if (distance >= DISTANCE_OF_VIEW)
            {
                c = 0;
            }
            else
            {
                c = 255 - (255 * distance) / DISTANCE_OF_VIEW;
            }

            Color shade = mix(Color.Brown, Color.SandyBrown, c, 255);

            return new Pen(shade);
        }


        private Image floorAndCielingImage = null;

        private Image getFloorAndCielingImage()
        {
            if (this.floorAndCielingImage == null)
            {
                int width = pnlViewport.Width;
                int height = pnlViewport.Height;

                this.floorAndCielingImage = new Bitmap(width, height);

                Graphics g = Graphics.FromImage(this.floorAndCielingImage);

                drawGradient(g, 0, height / 2, height / 2, width, Color.LightBlue, Color.DarkBlue);
                drawGradient(g, height / 2, height, height / 2, width, Color.Black, Color.DarkGreen);
            }

            return this.floorAndCielingImage;
        }


        private void paintScene(Graphics g)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Player player = Program.player;
            Map map = Program.map;

            int width = pnlViewport.Width;
            int height = pnlViewport.Height;

            // Clear screen
            //g.FillRectangle(new SolidBrush(Color.White), 0, 0, width, height);
            
            // Draw ceiling
            //Brush skyBrush = new SolidBrush(Color.LightBlue);
            //g.FillRectangle(skyBrush, 0, 0, width, height / 2);

            // Draw floor
            //Brush groundBrush = new SolidBrush(Color.Green);
            //g.FillRectangle(groundBrush, 0, height / 2, width, height);

            Image floorAndCielongImage = this.getFloorAndCielingImage();
            g.DrawImage(floorAndCielongImage, 0, 0);

            // Ray casting
            int cy = height / 2;

            for (int x = 0; x < width; x++)
            {
                int xOffset = x - width / 2;

                double angleOffset = xOffset * (Player.toRad(VIEWPORT_ANGLE) / width);
                double angle = Player.toRad(player.Angle) + angleOffset;

                Map.Intersection intersectionPoint = map.IntersectionWithWall(player.X, player.Y, angle);
                double distanceToWall = (intersectionPoint != null) ? intersectionPoint.DistanceTo(player.X, player.Y) : Double.PositiveInfinity;

                if (!Double.IsPositiveInfinity(distanceToWall))
                {
                    //double cos = Math.Cos(Math.Asin((double)xOffset / DISTANCE_TO_VIEWPORT));

                    //distanceToWall *= cos;

                    double hp = (player.Height * DISTANCE_TO_VIEWPORT) / distanceToWall;
                    double hq = DISTANCE_TO_VIEWPORT * (WALL_HEIGHT - player.Height) / distanceToWall;

                    this.DrawVLine(g,
                        intersectionPoint,
                        x,
                        cy - hq * 80.0,
                        cy + hp * 80.0);
                }
            }

            stopwatch.Stop();
            Console.WriteLine("stopwatch.Elapsed = " + stopwatch.Elapsed);
        }


        private void DrawVLine(Graphics g,
            Map.Intersection intersectionPoint,
            int x,
            double y1,
            double y2)
        {
            if (this.wallTexture == null)
            {
                this.wallTexture = new Bitmap("bricks.png");
                    //Image.FromFile("bricks.jpg", false);
            }

            int column =
                ( intersectionPoint.withVertical
                    ? Convert.ToInt32(intersectionPoint.Y)
                    : Convert.ToInt32(intersectionPoint.X) ) % Map.BLOCK_SIZE;

            int a1 = Convert.ToInt32(Math.Ceiling(y1));
            int a2 = Convert.ToInt32(Math.Floor(y2));

            Debug.Assert(a1 <= a2);

            int height = a2 - a1;

            g.DrawImage(this.wallTexture, 
                new Rectangle(x, a1, 1, height),
                column,
                0,
                1,
                this.wallTexture.Height,
                GraphicsUnit.Pixel);
        }


        private void DrawVLine(Graphics g,
            Pen pen,
            int x,
            double y1,
            double y2)
        {
            Debug.Assert(y1 <= y2);

            int a1 = Convert.ToInt32(Math.Ceiling(y1));
            int a2 = Convert.ToInt32(Math.Floor(y2));

            double r1 = a1 - y1;
            double r2 = y2 - a2;

            g.DrawLine(pen,
                        x,
                        a1,
                        x,
                        a2);

            if (this.bitSmoothWalls)
            {   
                if (r1 > 0)
                {
                    Int32 a = Convert.ToInt32(Math.Round(255.0 * r1)) & 0xFF;
                    Int32 argb = (pen.Color.ToArgb() & 0x00FFFFFF) | a << 24;

                    DrawPixel(g, Color.FromArgb(argb), x, a1 - 1);
                }

                if (r2 > 0)
                {
                    Int32 a = Convert.ToInt32(Math.Round(255.0 * r2)) & 0xFF;
                    Int32 argb = (pen.Color.ToArgb() & 0x00FFFFFF) | a << 24;

                    DrawPixel(g, Color.FromArgb(argb), x, a2 + 1);
                }
            }
        }


        private void DrawPixel(Graphics g, Color c, int x, int y)
        {
            Brush brush = new SolidBrush(c);

            g.FillRectangle(brush, x, y, 1, 1);

            brush.Dispose();
        }


        private static void drawGradient(Graphics g,
                                         int y1,
                                         int y2,
                                         int steps,
                                         int width,
                                         Color c1,
                                         Color c2)
        {
            Debug.Assert(y1 < y2);

            int height = y2 - y1 + 1;

            int tipLines = height % steps;
            int insertions = (tipLines > 0) ? (steps / tipLines) : 0;

            int ususalBarHeight = height / steps;

            int y = y1;

            for (int i = 0; i < steps; i++)
            {
                int barHeight = ususalBarHeight;

                if (insertions != 0 && (i % (height / insertions)) == 0)
                {
                    barHeight++;
                }

                Brush brush = new SolidBrush(mix(c1, c2, i + 1, steps));
                g.FillRectangle(brush, 0, y, width, y + barHeight);
                brush.Dispose();

                y += barHeight;
            }
        }


        private static int mix(int v1, int v2, int prop, int whole)
        {
            return (v1 * (whole - prop)) / whole + (v2 * prop) / whole;
        }


        private static Color mix(Color c1, Color c2, int prop, int whole)
        {
            return Color.FromArgb(
                    mix(c1.A, c2.A, prop, whole),
                    mix(c1.R, c2.R, prop, whole),
                    mix(c1.G, c2.G, prop, whole),
                    mix(c1.B, c2.B, prop, whole)
                );
        }


        private void pnlMap_Paint(object sender, PaintEventArgs e)
        {
            this.paintMap();

            //this.paintScene();
        }


        public static void drawArrow(Graphics g,
            Pen pen,
            int sx,
            int sy,
            int tx,
            int ty)
        {
            g.DrawLine(pen, sx, sy, tx, ty);

            int x1 = sx - tx,
                y1 = sy - ty;

            int x2 = x1,
                y2 = y1;

            x1 /= 4;
            y1 /= 4;
            x2 /= 4;
            y2 /= 4;

            rotate(ref x1, ref y1, +256, 0, 0);
            rotate(ref x2, ref y2, -256, 0, 0);

            g.DrawLine(pen, x1 + tx, y1 + ty, tx, ty);
            g.DrawLine(pen, x2 + tx, y2 + ty, tx, ty);
        }


        public static void rotate(ref double x, ref double y, double angle, double cx = 0, double cy = 0)
        {
            double xs = x - cx;
            double ys = y - cy;

            double sin = Math.Sin(angle);
            double cos = Math.Cos(angle);

            double xp = xs * cos - ys * sin;
            double yp = ys * cos + xs * sin;

            x = xp + cx;
            y = yp + cy;
        }


        public static void rotate(ref int x, ref int y, int angle, int cx = 0, int cy = 0)
        {
            double xx = x;
            double yy = y;

            rotate(ref xx, ref yy, big2rad(angle), cx, cy);

            x = (int) Math.Round(xx);
            y = (int) Math.Round(yy);
        }


        public static double big2rad(double binaryDegrees)
        {
            return binaryDegrees * (Math.PI / 2048.0);
        }


        private void pnlViewport_Paint(object sender, PaintEventArgs e)
        {
            //this.paintScene(sender, e);
            grafx.Render(e.Graphics);
        }


        private void chbSmoothWalls_CheckedChanged(object sender, EventArgs e)
        {
            this.bitSmoothWalls = !this.bitSmoothWalls;

            this.pnlViewport.Refresh();
        }

        private void pnlMap_MouseClick(object sender, MouseEventArgs e)
        {
            int col = e.X * Program.map.width / pnlMap.Width;
            int row = e.Y * Program.map.height / pnlMap.Height;

            if (!Program.map.isDefined(col, row))
            {
                return;
            }

            if (Program.map.isBlockWall(col * Map.BLOCK_SIZE, row * Map.BLOCK_SIZE))
            {
                Program.map[col, row] = 0;
            }
            else
            {
                Program.map[col, row] = 1;
            }

            this.paintScene(grafx.Graphics);

            this.pnlViewport.Refresh();
            this.pnlMap.Refresh();
        }
    }
}
