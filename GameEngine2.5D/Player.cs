﻿//
// Written by Andrey Zhmakin, 2015
//

using System;
using System.Diagnostics;

namespace Flat3D
{
    class Player
    {
        public const int TURN_STEP = 16;
        public const int STEP = 2;
        public const int INIT_HEIGHT = 8;


        private double x = Map.BLOCK_SIZE * 2;

        public double X
        {
            get
            {
                return this.x;
            }

            set
            {
                this.x = value;
            }
        }

        /*public int X
        {
            get
            {
                return Convert.ToInt32(Math.Round(this.x));
            }

            set
            {
                this.x = value;
            }
        }*/


        private double y = Map.BLOCK_SIZE * 2;

        public double Y
        {
            get
            {
                return this.y;
            }

            set
            {
                this.y = value;
            }
        }

        /*public int Y
        {
            get
            {
                return Convert.ToInt32(Math.Round(this.y));
            }

            set
            {
                this.y = value;
            }
        }*/


        public int Height = INIT_HEIGHT;


        private int angle = 0;

        public int Angle
        {
            get
            {
                return this.angle;
            }

            set
            {
                Debug.Assert(false);
                this.angle = value;
            }
        }


        public void turnLeft()
        {
            angle = (4096 + angle - TURN_STEP) % 4096;
        }


        public void turnRight()
        {
            angle = (angle + TURN_STEP) % 4096;
        }


        public void moveForward(Map map)
        {
            double tx = this.x + STEP;
            double ty = this.y;

            frmMain.rotate(ref tx, ref ty, frmMain.big2rad(this.angle), this.x, this.y);

            if ( !map.isBlockWall(Convert.ToInt32(Math.Round(tx)), Convert.ToInt32(Math.Round(ty))) )
            {
                this.x = tx;
                this.y = ty;
            }
        }


        public void moveBackward(Map map)
        {
            double tx = this.x - STEP;
            double ty = this.y;

            frmMain.rotate(ref tx, ref ty, frmMain.big2rad(this.angle), this.x, this.y);

            if ( !map.isBlockWall(Convert.ToInt32(Math.Round(tx)), Convert.ToInt32(Math.Round(ty))) )
            {
                this.x = tx;
                this.y = ty;
            }
        }


        public void StrafeLeft(Map map)
        {
            angle -= 1024;
            this.moveForward(map);
            angle += 1024;
        }


        public void StrafeRight(Map map)
        {
            angle += 1024;
            this.moveForward(map);
            angle -= 1024;
        }


        public static double toRad(int value)
        {
            return (2.0 * Math.PI / 4096.0) * value;
        }


        public static double fromRad(int value)
        {
            return (4096.0 / (2.0 * Math.PI)) * value;
        }
    }
}
