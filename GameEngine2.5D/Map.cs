﻿//
// Written by Andrey Zhmakin, 2015
//

using System;
using System.Diagnostics;
using Flat3D.Geometry;

namespace Flat3D
{
    class Map
    {
        public const int BLOCK_SIZE = 64;

        private int[,] map =
                new int[,]
                {
                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                    { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                    { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                    { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                    { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                    { 1, 0, 0, 0, 0, 1, 1, 0, 0, 1 },
                    { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                    { 1, 0, 0, 0, 0, 1, 1, 0, 0, 1 },
                    { 1, 0, 1, 0, 0, 1, 0, 0, 0, 1 },
                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
                };

        public int width;
        public int height;

        Player player;

        public Map(Player player)
        {
            this.player = player;
            this.width = this.map.GetLength(0);
            this.height = this.map.GetLength(1);
        }


        public bool isBlockWall(int x, int y)
        {
            return this.block(x / BLOCK_SIZE, y / BLOCK_SIZE) == 1;
        }


        public int block(int x, int y)
        {
            return this.isDefined(x, y) ? this.map[y, x] : 0;
        }


        public bool isDefined(int x, int y)
        {
            return x >= 0 && x < this.width && y >= 0 && y < this.height;
        }


        public double distanceToWall(int x, int y, double angle)
        {
            Point2D intersection = this.IntersectionWithWall(x, y, angle);

            return (intersection != null) ? intersection.DistanceTo(x, y) : Double.PositiveInfinity;
        }


        public class Intersection : Point2D
        {
            public bool withVertical;

            public Intersection(Point2D point, bool withVertical)
                : base(point)
            {
                this.withVertical = withVertical;
            }
        }


        public Intersection IntersectionWithWall(double x, double y, double angle)
        {
            //angle = IntGeometry.normalizeDegrees(angle);

            angle = Normalize(angle);

            Point2D intersectionX = this.scan(x, y, angle, true);
            Point2D intersectionY = this.scan(x, y, angle, false);

            if (intersectionX == null && intersectionY == null)
            {
                return null;
            }

            double distanceX = (intersectionX != null) ? intersectionX.DistanceTo(x, y) : Double.PositiveInfinity;
            double distanceY = (intersectionY != null) ? intersectionY.DistanceTo(x, y) : Double.PositiveInfinity;

            return (distanceX < distanceY) ? new Intersection(intersectionX, true) : new Intersection(intersectionY, false);
        }

        
        private Point2D scan(double startX,
                             double startY,
                             double angle,
                             bool withVertical)
        {
            Debug.Assert(angle >= 0 && angle < Math.PI * 2.0);

            if (withVertical)
            {
                if (angle == 1.5 * Math.PI || angle == 0.5 * Math.PI)
                {
                    return null;
                }

                bool increasing = !(angle > 0.5 * Math.PI && angle < 1.5 * Math.PI);
                int descending = 0;

                if (angle > 0 && angle < Math.PI)
                {
                    descending = +1;
                }
                else if (angle > Math.PI && angle < 2.0 * Math.PI)
                {
                    descending = -1;
                }

                double k = Math.Tan(angle);

                if (Double.IsNaN(k) || Double.IsInfinity(k))
                {
                    return null;
                }

                double b = startY - k * startX;

                int intStartX = Convert.ToInt32(Math.Round(startX));

                int line = intStartX / BLOCK_SIZE;

                if (intStartX % BLOCK_SIZE != 0)
                {
                    line += increasing ? +1 : 0;
                }

                int lastLine = increasing ? this.width : -1;

                for (; increasing ? (line < lastLine) : (line > lastLine); line += increasing ? +1 : -1)
                {
                    int x = line * BLOCK_SIZE;
                    double dY = k * x + b;
                    int y;

                    if (dY < 0 || dY >= this.height * BLOCK_SIZE)
                    {
                        return null;
                    }

                    try
                    {
                        y = Convert.ToInt32(Math.Floor(dY));
                    }
                    catch (OverflowException)
                    {
                        //Console.WriteLine("V: Outside the range of the Int32 type.");
                        return null;
                    }

                    int py = y;

                    int testX = 0;
                    int testY = 0;

                    if (!increasing)
                    {
                        testX = -1;
                    }

                    if (Math.Abs(y - dY) <= 1.0e-10) //(y % BLOCK_SIZE == 0)
                    {
                        testY = descending;
                    }

                    if (this.isBlockWall(x + testX, py + testY))
                    {
                        return new Point2D(x, dY);
                    }
                }
            }
            else
            {
                if (angle == 0 || angle == Math.PI)
                {
                    return null;
                }

                bool increasing = (angle > 0 && angle < Math.PI);

                double k = Math.Tan(-(angle - 0.5 * Math.PI));

                if (Double.IsNaN(k) || Double.IsInfinity(k))
                {
                    return null;
                }

                double b = startX - k * startY;

                int intStartY = Convert.ToInt32(Math.Round(startY));

                int line = intStartY / BLOCK_SIZE;

                if (intStartY % BLOCK_SIZE != 0)
                {
                    line += increasing ? +1 : 0;
                }

                int lastLine = increasing ? this.height : -1;

                for (; increasing ? (line < lastLine) : (line > lastLine); line += increasing ? +1 : -1)
                {
                    int y = line * BLOCK_SIZE;
                    double dX = k * y + b;
                    int x;

                    if (dX < 0 || dX >= this.width * BLOCK_SIZE)
                    {
                        return null;
                    }

                    try
                    {
                        x = Convert.ToInt32(Math.Floor(dX));
                    }
                    catch (OverflowException)
                    {
                        //Console.WriteLine("H: Outside the range of the Int32 type.");
                        return null;
                    }

                    int testX = 0;
                    int testY = 0;

                    if (!increasing)
                    {
                        testY = -1;
                    }

                    if (x % BLOCK_SIZE == 0)
                    {
                        
                    }

                    if (this.isBlockWall(x + testX, y + testY))
                    {
                        return new Point2D(dX, y);
                    }
                }
            }

            return null;
        }

        private static double Normalize(double angle)
        {
            while (angle < 0 || angle >= 2.0 * Math.PI)
            {
                if (angle < 0)
                {
                    angle += 2.0 * Math.PI;
                }
                else if (angle >= 2.0 * Math.PI)
                {
                    angle -= 2.0 * Math.PI;
                }
            }

            return angle;
        }

        public int this[int x, int y]
        {
            get
            {
                return this.map[y, x];
            }
            set
            {
                this.map[y, x] = value;
            }
        }
    }
}
