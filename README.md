Game Engine 2.5D
By Andrey Zhmakin

![GameEngine2.5D.jpg](https://bitbucket.org/repo/neaxqy/images/1422951998-GameEngine2.5D.jpg)

This project is a demonstrator of ray casting algorithm, hence 2.5D. This algorithm was used in the dawn of 3D shooting game era.

Use arrow and WASD keys to move. Click on the mini map to alter walls.

This will never become anything more than that because it is just an algorithm demonstrator. ;-)